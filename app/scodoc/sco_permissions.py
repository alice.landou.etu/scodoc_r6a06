# -*- mode: python -*-
# -*- coding: utf-8 -*-

"""Definition of ScoDoc permissions
    used by auth
"""

# Définition des permissions: NE PAS CHANGER les numéros ou l'ordre des lignes !
# Les permissions sont sur un BigInt en base SQL, donc 64 bits.
_SCO_PERMISSIONS = (
    # permission bit, symbol, description
    # ScoSuperAdmin est utilisé pour:
    #   - add/delete departments
    #   - tous rôles lors creation utilisateurs
    (1 << 1, "ScoSuperAdmin", "Super Administrateur"),
    (1 << 2, "ScoView", "Voir"),
    (1 << 3, "EnsView", "Voir les parties pour les enseignants"),
    (1 << 4, "Observateur", "Observer (accès lecture restreint aux bulletins)"),
    (1 << 5, "UsersAdmin", "Gérer les utilisateurs (de son département)"),
    (1 << 6, "UsersView", "Voir les utilisateurs (de tous les départements)"),
    (1 << 7, "EditPreferences", "Modifier les préférences"),
    (1 << 8, "EditFormation", "Changer les formations"),
    (1 << 9, "EditFormationTags", "Tagguer les formations"),
    (1 << 10, "EditAllNotes", "Modifier toutes les notes"),
    (1 << 11, "EditAllEvals", "Modifier toutes les évaluations"),
    (1 << 12, "EditFormSemestre", "Mettre en place une formation (créer un semestre)"),
    (1 << 13, "AbsChange", "Saisir des absences ou justificatifs"),
    (1 << 14, "AbsAddBillet", "Saisir des billets d'absences"),
    # changer adresse/photo ou pour envoyer bulletins par mail ou pour debouche
    (1 << 15, "EtudChangeAdr", "Changer les adresses d'étudiants"),
    (
        1 << 16,
        "APIEditGroups",
        "API: Modifier les groupes (obsolete, use EtudChangeGroups)",
    ),
    (1 << 16, "EtudChangeGroups", "Modifier les groupes"),
    # aussi pour demissions, diplomes:
    (1 << 17, "EtudInscrit", "Inscrire des étudiants"),
    # aussi pour archives:
    (
        1 << 18,
        "EtudAddAnnotations",
        "Éditer les annotations (et fichiers) sur étudiants",
    ),
    # inutilisée (1 << 19, "ScoEntrepriseView", "Voir la section 'entreprises'"),
    # inutilisée  (1 << 20, "EntrepriseChange", "Modifier les entreprises"),
    # XXX inutilisée ? (1 << 21, "EditPVJury", "Éditer les PV de jury"),
    # ajouter maquettes Apogee (=> chef dept et secr):
    (1 << 22, "EditApogee", "Gérer les exports Apogée"),
    # Application relations entreprises
    (1 << 23, "RelationsEntrepView", "Voir l'application relations entreprises"),
    (1 << 24, "RelationsEntrepEdit", "Modifier les entreprises"),
    (1 << 25, "RelationsEntrepSend", "Envoyer des offres"),
    (1 << 26, "RelationsEntrepValidate", "Valide les entreprises"),
    (1 << 27, "RelationsEntrepViewCorrs", "Voir les correspondants"),
    (
        1 << 28,
        "RelationsEntrepExport",
        "Exporter les données de l'application relations entreprises",
    ),
    (1 << 29, "UsersChangeCASId", "Paramétrer l'id CAS"),
    (1 << 30, "ViewEtudData", "Accéder aux données personnelles des étudiants"),
    #
    # XXX inutilisée ? (1 << 40, "EtudChangePhoto", "Modifier la photo d'un étudiant"),
    # Permissions du module Assiduité)
    (
        1 << 50,
        "AbsJustifView",
        "Visualisation du détail des justificatifs (motif, fichiers)",
    ),
    # Attention: les permissions sont codées sur 64 bits.
)


class Permission:
    "Permissions for ScoDoc"
    NBITS = 1  # maximum bits used (for formatting)
    ALL_PERMISSIONS = [-1]
    description = {}  # { symbol : blah blah }
    permission_by_name = {}  # { symbol : int }
    permission_by_value = {}  # { int : symbol }

    @staticmethod
    def init_permissions():
        for perm, symbol, description in _SCO_PERMISSIONS:
            setattr(Permission, symbol, perm)
            Permission.description[symbol] = description
            Permission.permission_by_name[symbol] = perm
            Permission.permission_by_value[perm] = symbol
        max_perm = max(p[0] for p in _SCO_PERMISSIONS)
        Permission.NBITS = max_perm.bit_length()

    @staticmethod
    def get_by_name(permission_name: str) -> int:
        """Return permission mode (integer bit field), or None if it doesn't exist."""
        return Permission.permission_by_name.get(permission_name)

    @staticmethod
    def get_name(permission: int) -> str:
        """Return permission name, or None if it doesn't exist."""
        return Permission.permission_by_value.get(permission)

    @staticmethod
    def permissions_names(permissions: int) -> list[str]:
        """From a bit field, return list of permission names"""
        names = []
        if permissions == 0:
            return []
        mask = 1 << (permissions.bit_length() - 1)
        while mask > 0:
            if mask & permissions:
                name = Permission.get_name(mask)
                if name is not None:
                    names.append(name)
            mask = mask >> 1
        return names


Permission.init_permissions()
