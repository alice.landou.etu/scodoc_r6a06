# -*- mode: python -*-
# -*- coding: utf-8 -*-

##############################################################################
#
# Gestion scolarite IUT
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""Calcul des moyennes de module (restes de fonctions ScoDoc 7)
"""
from app.models import ModuleImpl
import app.scodoc.notesdb as ndb


def moduleimpl_has_expression(modimpl: ModuleImpl):
    """True if we should use a user-defined expression
    En ScoDoc 9, utilisé pour afficher un avertissement, l'expression elle même
    n'est plus supportée.
    """
    return (
        modimpl.computation_expr
        and modimpl.computation_expr.strip()
        and modimpl.computation_expr.strip()[0] != "#"
    )


def formsemestre_expressions_use_abscounts(formsemestre_id):
    """True si les notes de ce semestre dépendent des compteurs d'absences.
    Cela n'est normalement pas le cas, sauf si des formules utilisateur
    utilisent ces compteurs.
    """
    # check presence of 'nbabs' in expressions
    ab = "nb_abs"  # chaine recherchée
    cnx = ndb.GetDBConnexion()
    # 1- moyennes d'UE:
    elist = formsemestre_ue_computation_expr_list(
        cnx, {"formsemestre_id": formsemestre_id}
    )
    for e in elist:
        expr = e["computation_expr"].strip()
        if expr and expr[0] != "#" and ab in expr:
            return True
    # 2- moyennes de modules
    # #sco9 il n'y a plus d'expressions
    # for mod in sco_moduleimpl.moduleimpl_list(formsemestre_id=formsemestre_id):
    #    if moduleimpl_has_expression(mod) and ab in mod["computation_expr"]:
    #        return True
    return False


_formsemestre_ue_computation_exprEditor = ndb.EditableTable(
    "notes_formsemestre_ue_computation_expr",
    "notes_formsemestre_ue_computation_expr_id",
    (
        "notes_formsemestre_ue_computation_expr_id",
        "formsemestre_id",
        "ue_id",
        "computation_expr",
    ),
    html_quote=False,  # does nt automatically quote
)
formsemestre_ue_computation_expr_create = _formsemestre_ue_computation_exprEditor.create
formsemestre_ue_computation_expr_delete = _formsemestre_ue_computation_exprEditor.delete
formsemestre_ue_computation_expr_list = _formsemestre_ue_computation_exprEditor.list
formsemestre_ue_computation_expr_edit = _formsemestre_ue_computation_exprEditor.edit


def get_ue_expression(formsemestre_id, ue_id, html_quote=False):
    """Returns UE expression (formula), or None if no expression has been defined"""
    cnx = ndb.GetDBConnexion()
    el = formsemestre_ue_computation_expr_list(
        cnx, {"formsemestre_id": formsemestre_id, "ue_id": ue_id}
    )
    if not el:
        return None
    else:
        expr = el[0]["computation_expr"].strip()
        if expr and expr[0] != "#":
            if html_quote:
                expr = ndb.quote_html(expr)
            return expr
        else:
            return None
