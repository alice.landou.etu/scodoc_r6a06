# -*- coding: UTF-8 -*

from flask import render_template
from app.auth.models import User
from app.email import get_from_addr, send_email


def send_password_reset_email(user: User):
    """Send message allowing to reset password"""
    recipients = user.get_emails()
    if not recipients:
        return
    token = user.get_reset_password_token()
    send_email(
        "[ScoDoc] Réinitialisation de votre mot de passe",
        sender=get_from_addr(),
        recipients=recipients,
        text_body=render_template("email/reset_password.txt", user=user, token=token),
        html_body=render_template("email/reset_password.j2", user=user, token=token),
    )
