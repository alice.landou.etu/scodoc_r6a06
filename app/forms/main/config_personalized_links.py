"""
Formulaire configuration liens personalisés (menu "Liens")
"""

from flask_wtf import FlaskForm
from wtforms import validators
from wtforms.fields.simple import BooleanField, StringField, SubmitField

from app.models import ScoDocSiteConfig


class _PersonalizedLinksForm(FlaskForm):
    "form. définition des liens personnalisés"
    # construit dynamiquement ci-dessous


def PersonalizedLinksForm() -> _PersonalizedLinksForm:
    "Création d'un formulaire pour éditer les liens"

    # Formulaire dynamique, on créé une classe ad-hoc
    class F(_PersonalizedLinksForm):
        pass

    F.links_by_id = dict(enumerate(ScoDocSiteConfig.get_perso_links()))

    def _gen_link_form(idx):
        setattr(
            F,
            f"link_{idx}",
            StringField(
                "Titre",
                validators=[
                    validators.Optional(),
                    validators.Length(min=1, max=80),
                ],
                default="",
                render_kw={"size": 6},
            ),
        )
        setattr(
            F,
            f"link_url_{idx}",
            StringField(
                "URL",
                description="adresse, incluant le http.",
                validators=[
                    validators.Optional(),
                    validators.URL(),
                    validators.Length(min=1, max=256),
                ],
                default="",
            ),
        )
        setattr(
            F,
            f"link_with_args_{idx}",
            BooleanField(
                "ajouter arguments",
                description="query string avec ids",
            ),
        )

    # Initialise un champ de saisie par lien
    for idx in F.links_by_id:
        _gen_link_form(idx)
    _gen_link_form("new")

    F.submit = SubmitField("Valider")
    F.cancel = SubmitField("Annuler", render_kw={"formnovalidate": True})

    return F()
